/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <MovePlugin.hh>
#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;

PYBIND11_EMBEDDED_MODULE(Move, m) {

  QObject* pluginPointer = getPluginPointer("Move");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-Move" << std::endl;
     return;
   }

  MovePlugin* plugin = qobject_cast<MovePlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-Move" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< MovePlugin,std::unique_ptr<MovePlugin, py::nodelete> > move(m, "Move");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  move.def(py::init([plugin]() { return plugin; }));

  move.def("moveToOrigin",  &MovePlugin::slotMoveToOrigin,
                                         QCoreApplication::translate("PythonDocMove","Move all target Meshes cog to the origin").toLatin1().data()  );

  move.def("unifyBoundingBoxDiagonal",  &MovePlugin::slotUnifyBoundingBoxDiagonal,
                                         QCoreApplication::translate("PythonDocMove","Unify bounding box diagonal of all target meshes").toLatin1().data()  );

  move.def("unifyBoundingBoxLongestAxis",  &MovePlugin::slotUnifyBoundingBoxLongestAxis,
                                         QCoreApplication::translate("PythonDocMove","Scale bounding box of all target meshes such that longest axis has unit size (keeps aspect ratio)").toLatin1().data()  );

  move.def("unifyBoundingBoxAllAxis",  &MovePlugin::slotUnifyBoundingBoxAllAxis,
                                         QCoreApplication::translate("PythonDocMove","Scale bounding box of all target meshes such that all axis have unit size").toLatin1().data()  );

  move.def("translate",   static_cast<void (MovePlugin::*)(int,Vector)>(&MovePlugin::translate),
                                         QCoreApplication::translate("PythonDocMove","Translate object by given vector.").toLatin1().data(),
                                 py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                 py::arg(QCoreApplication::translate("PythonDocMove","Translation vector").toLatin1().data()) );

  move.def("translate",   static_cast<void (MovePlugin::*)(int,IdList,Vector)>(&MovePlugin::translate),
                                         QCoreApplication::translate("PythonDocMove","Translate vertices of an object by given vector.").toLatin1().data(),
                                 py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                 py::arg(QCoreApplication::translate("PythonDocMove","List of vertex indices to be moved").toLatin1().data()),
                                 py::arg(QCoreApplication::translate("PythonDocMove","Translation vector").toLatin1().data()) );

  move.def("translateVertexSelection",   static_cast<void (MovePlugin::*)(int,Vector)>(&MovePlugin::translateVertexSelection),
                                          QCoreApplication::translate("PythonDocMove","Translate current vertex selection of an object by given vector.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Translation vector").toLatin1().data()) );



  move.def("translateFaceSelection",   static_cast<void (MovePlugin::*)(int,Vector)>(&MovePlugin::translateFaceSelection),
                                          QCoreApplication::translate("PythonDocMove","Translate current face selection of an object by given vector.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Translation vector").toLatin1().data()) );

  move.def("translateEdgeSelection",   static_cast<void (MovePlugin::*)(int,Vector)>(&MovePlugin::translateEdgeSelection),
                                          QCoreApplication::translate("PythonDocMove","Translate current edge selection of an object by given vector.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Translation vector").toLatin1().data()) );

  move.def("transform",   static_cast<void (MovePlugin::*)(int,Matrix4x4)>(&MovePlugin::transform),
                                          QCoreApplication::translate("PythonDocMove","Transform an object with a 4x4 matrix").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Transformation matrix").toLatin1().data()) );

  move.def("transform",   static_cast<void (MovePlugin::*)(int,IdList,Matrix4x4)>(&MovePlugin::transform),
                                          QCoreApplication::translate("PythonDocMove","Transform an object with a 4x4 matrix").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","List of vertex handles to move").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Transformation matrix").toLatin1().data()) );

  move.def("transformVertexSelection",   static_cast<bool (MovePlugin::*)(int,Matrix4x4)>(&MovePlugin::transformVertexSelection),
                                          QCoreApplication::translate("PythonDocMove","Transform selected vertices by given matrix.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Transformation matrix").toLatin1().data()) );


  move.def("transformFaceSelection",   static_cast<bool (MovePlugin::*)(int,Matrix4x4)>(&MovePlugin::transformFaceSelection),
                                          QCoreApplication::translate("PythonDocMove","Transform selected faces by given matrix.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Transformation matrix").toLatin1().data()) );

  move.def("transformEdgeSelection",   static_cast<bool (MovePlugin::*)(int,Matrix4x4)>(&MovePlugin::transformEdgeSelection),
                                          QCoreApplication::translate("PythonDocMove","Transform selected edges by given matrix.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Transformation matrix").toLatin1().data()) );

  move.def("transformCellSelection",   static_cast<bool (MovePlugin::*)(int,Matrix4x4)>(&MovePlugin::transformCellSelection),
                                          QCoreApplication::translate("PythonDocMove","Transform selected cells by given matrix.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Transformation matrix").toLatin1().data()) );

  move.def("transformHandleRegion",   static_cast<void (MovePlugin::*)(int,Matrix4x4)>(&MovePlugin::transformHandleRegion),
                                          QCoreApplication::translate("PythonDocMove","Transform handle region by given matrix.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Transformation matrix").toLatin1().data()) );

  move.def("setManipulatorPosition",   &MovePlugin::setManipulatorPosition,
                                          QCoreApplication::translate("PythonDocMove","Set the position of the manipulator.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","3D position").toLatin1().data()) );

  move.def("manipulatorPosition",  &MovePlugin::manipulatorPosition,
                                          QCoreApplication::translate("PythonDocMove","Get the position of the manipulator.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()) );

  move.def("setManipulatorDirection",   &MovePlugin::setManipulatorDirection,
                                          QCoreApplication::translate("PythonDocMove","Set the direction of the manipulator.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","X-Axis direction").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Y-Axis direction").toLatin1().data()));

  move.def("manipulatorDirectionX",  &MovePlugin::manipulatorDirectionX,
                                          QCoreApplication::translate("PythonDocMove","Get the x-direction of an object's manipulator.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()) );

  move.def("manipulatorDirectionY",  &MovePlugin::manipulatorDirectionY,
                                          QCoreApplication::translate("PythonDocMove","Get the y-direction of an object's manipulator.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()) );

  move.def("manipulatorDirectionZ",  &MovePlugin::manipulatorDirectionZ,
                                          QCoreApplication::translate("PythonDocMove","Get the z-direction of an object's manipulator.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()) );

  move.def("objectRenderingMatrixIdentity",  &MovePlugin::objectRenderingMatrixIdentity,
                                          QCoreApplication::translate("PythonDocMove","Resets  the objects rendering matrix to identity.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()) );

  move.def("objectRenderingMatrixScale",   &MovePlugin::objectRenderingMatrixScale,
                                          QCoreApplication::translate("PythonDocMove","Adds a scaling factor to the Object rendering Matrix in the scenegraph.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","scaling factor").toLatin1().data()) );

  move.def("objectRenderingMatrixTranslate",   &MovePlugin::objectRenderingMatrixTranslate,
                                          QCoreApplication::translate("PythonDocMove","Adds a translation to the Object rendering Matrix in the scenegraph.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Translation vector").toLatin1().data()) );

  move.def("objectRenderingMatrixRotate",   &MovePlugin::objectRenderingMatrixRotate,
                                          QCoreApplication::translate("PythonDocMove","Adds a rotation to the Object rendering Matrix in the scenegraph.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Rotation axis").toLatin1().data()),
                                  py::arg(QCoreApplication::translate("PythonDocMove","Rotation angle").toLatin1().data()) );

  move.def("getObjectRenderingMatrix",   &MovePlugin::getObjectRenderingMatrix,
                                          QCoreApplication::translate("PythonDocMove","Returns the current object transformation matrix from the Scenegraph.").toLatin1().data(),
                                  py::arg(QCoreApplication::translate("PythonDocMove","ID of an object").toLatin1().data()));

}

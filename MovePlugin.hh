/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#pragma once

#include <QObject>
#include <QMenuBar>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/MouseInterface.hh>
#include <OpenFlipper/BasePlugin/KeyInterface.hh>
#include <OpenFlipper/BasePlugin/PickingInterface.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/BasePlugin/BackupInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>
#include <OpenFlipper/BasePlugin/ToolbarInterface.hh>
#include <OpenFlipper/BasePlugin/ContextMenuInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/PythonInterface.hh>
#include <OpenFlipper/BasePlugin/RPCInterface.hh>
#include <OpenFlipper/common/Types.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <ObjectTypes/Plane/Plane.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

#ifdef ENABLE_SKELETON_SUPPORT
  #include <ObjectTypes/Skeleton/Skeleton.hh>
#endif

#if defined(ENABLE_HEXAHEDRALMESH_SUPPORT) || defined(ENABLE_POLYHEDRALMESH_SUPPORT) || defined(ENABLE_TETRAHEDRALMESH_SUPPORT)
#include <OpenVolumeMesh/Attribs/NormalAttrib.hh>
#endif

#include "MoveToolbar.hh"
#include "MoveProps.hh"
#include "MoveObjectMarker.hh"

#include <ACG/QtScenegraph/QtTranslationManipulatorNode.hh>

/** Plugin for moving objects and selections
*/
class MovePlugin : public QObject, BaseInterface, MouseInterface, KeyInterface, PickingInterface, ToolboxInterface, BackupInterface, LoggingInterface, ScriptInterface,ToolbarInterface, PythonInterface, ContextMenuInterface, LoadSaveInterface, RPCInterface
{
  Q_OBJECT
  Q_INTERFACES(BaseInterface)
  Q_INTERFACES(MouseInterface)
  Q_INTERFACES(KeyInterface)
  Q_INTERFACES(PickingInterface)
  Q_INTERFACES(ToolboxInterface)
  Q_INTERFACES(BackupInterface)
  Q_INTERFACES(LoggingInterface)
  Q_INTERFACES(ContextMenuInterface)
  Q_INTERFACES(ScriptInterface)
  Q_INTERFACES(ToolbarInterface)
  Q_INTERFACES(LoadSaveInterface)
  Q_INTERFACES(RPCInterface)
  Q_INTERFACES(PythonInterface)

  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-Move")

public:
  typedef unsigned int SelectionType;
  static const SelectionType VERTEX = 1;
  static const SelectionType EDGE   = 2;
  static const SelectionType FACE   = 4;
  static const SelectionType CELL   = 8;

  signals:
    // BaseInterface
    void updateView() override;
    void updatedObject(int, const UpdateType& _type) override;
    void nodeVisibilityChanged(int) override;

    // PickingInterface
    void addPickMode( const std::string& _mode ) override;
    void addHiddenPickMode( const std::string& _mode ) override;
    void setPickModeMouseTracking (const std::string& _mode, bool _mouseTracking) override;
    void setPickModeToolbar (const std::string& _mode, QToolBar * _toolbar) override;

    // ContextMenuInterface
    void addContextMenuItem(QAction* _action , ContextMenuType _type) override;

    // BackupInterface
    void createBackup( int _objectid, QString _name, UpdateType _type = UPDATE_ALL) override;

    // LoggingInterface
    void log(Logtype _type, QString _message) override;
    void log(QString _message) override;

    // ScriptInterface
    void scriptInfo( QString _functionName  );
    void setSlotDescription(QString     _slotName,   QString     _slotDescription,
                            QStringList _parameters, QStringList _descriptions) override;

    // ToolbarInterface
    void addToolbar(QToolBar* _toolbar) override;
    void getToolBar( QString _name, QToolBar*& _toolbar) override;

    // KeyInterface
    void registerKey(int _key, Qt::KeyboardModifiers _modifiers, QString _description, bool _multiUse = false) override;
    
    // ToolboxInterface
    void addToolbox( QString _name  , QWidget* _widget, QIcon* _icon ) override;
    
    // RPCInterface
    void pluginExists(QString _pluginName, bool &_exists) override;
    void functionExists(QString _pluginName, QString _functionName, bool &_exists) override;

  private slots :

    // BaseInterface
    void initializePlugin() override;
    void pluginsInitialized() override;
    
    void slotAllCleared() override;
    
    // LoadSaveInterface
    void objectDeleted( int _id ) override;

    // MouseInterface
    void slotMouseWheelEvent(QWheelEvent * _event, const std::string & _mode) override;
    void slotMouseEvent( QMouseEvent* _event ) override;

    // KeyInterface
    void slotKeyEvent (QKeyEvent* _event) override;
    void slotKeyReleaseEvent (QKeyEvent* _event) override;

    // PickingInterface
    void slotPickModeChanged( const std::string& _mode) override;

  public :

    /// Default Constructor
    MovePlugin();

    /// Destructor
    ~MovePlugin();

    /// Name of the Plugin
    QString name() override { return (QString("Move")); };

    /// Description of the Plugin
    QString description() override { return (QString(tr("Moves Meshes with Manipulators"))); };

//===========================================================================
/** @name ToolBox
  * @{ */
//===========================================================================

  private:
    /// stores the current axes in the tool
    int axisA_;
    int axisB_;

    enum Unificationtype
    {
      DIAGONAL,
      LONGEST_AXIS,
      ALL_AXIS
    };

  private slots:

    /// Position of manipulator in tab changed
    void slotSetPosition();

    /// Toggle the first axis for changing direction in tab
    void slotToggleAxisA();

    /// Toggle the second axis for changing direction in tab
    void slotToggleAxisB();

    /// Set Direction of manipulator in tab changed
    void slotSetDirection();

    /// perform a translation for Manipulator in tab
    void slotTranslation();

    /// Project the current manipulator onto the tangent plane of the object
    void slotProjectToTangentPlane();

    /// Move the current manipulator to the cog of the object
    void slotMoveManipToCOG();

    /// Rotate Manipulator (with values from Tab)
    void slotRotate();

    /// Scale (with values from Tab)
    void slotScale();

    void slotEnableSelectionMode();

    void slotEnableObjectMode();

  private:

    /// Update the Dialog with the last clicked manipulator
    void updateManipulatorDialog();

    /// Get closest vertex to hitpoint
    template <typename MeshType>
    OpenMesh::Vec3d getNearestVertex(MeshType* _mesh, uint _fh, OpenMesh::Vec3d& _hitPoint);

    /// Get closest edge to hitpoint
    template <typename MeshType>
    OpenMesh::Vec3d getNearestEdge(MeshType* _mesh, uint _fh, OpenMesh::Vec3d& _hitPoint);

    /// Get closest face to hitpoint
    template <typename MeshType>
    OpenMesh::Vec3d getNearestFace(MeshType* _mesh, uint _fh, OpenMesh::Vec3d& _hitPoint);

    /// True if the toolbox widget is active
    bool toolboxActive_;

    /// Widget for Toolbox
    moveToolbarWidget* tool_;
    
    QIcon* toolIcon_;

/** @} */

//===========================================================================
/** @name ToolBar
  * @{ */
//===========================================================================

  private :
    QAction* moveAction_;
    QAction* moveSelectionAction_;
    
    QActionGroup* toolBarActions_;
    
    QToolBar* toolbar_;

  private slots:

    /// Called by Toolbar to enable move mode
    void slotSetMoveMode(QAction* _action);

/** @} */

//===========================================================================
/** @name PickToolBar
  * @{ */
//===========================================================================

  private :
    QToolBar* pickToolbar_;

    QAction* placeAction_;
    QAction* rotateTranslateAction_;
    QAction* rotateManipAction_;
    QAction* resizeAction_;

    QAction* biggerManipAction_;
    QAction* smallerManipAction_;

    QAction* fixChildManipAction_;
    QAction* transformRefPoseManipAction_;
    QAction* currentPoseManipAction_;
    
    QAction* placeAndSnapAction_;

    QActionGroup* pickToolBarActions_;

  private slots:

    /// Called by pick Toolbar
    void slotPickToolbarAction(QAction* _action);

/** @} */

//===========================================================================
/** @name Manipulator Handling
  * @{ */
//===========================================================================

  private:
    //object ids of all objects with active Manipulator
    std::vector< int > activeManipulators_;

  private slots:

    /// move the object when its manipulator moves
    void manipulatorMoved( QtTranslationManipulatorNode* _node , QMouseEvent* _event);

    /// update object when its manipulator changes position
    void ManipulatorPositionChanged(QtTranslationManipulatorNode* _node );

  private:

    void unifyBoundingBox(Unificationtype u);

    ///Transform a mesh with the given transformation matrix
    template< typename MeshT >
    void transformMesh(ACG::Matrix4x4d _mat , MeshT& _mesh );

    #ifdef ENABLE_POLYLINE_SUPPORT
    ///Transform a polyline with the given transformation matrix
    template< class PolyLineT >
    void transformPolyLine( ACG::Matrix4x4d _mat , PolyLineT& _polyLine );
    #endif

    #ifdef ENABLE_SKELETON_SUPPORT
    ///Transform a skeleton with the given transformation matrix
    void transformSkeleton( ACG::Matrix4x4d _mat , Skeleton& _skeleton );
    #endif

    #if defined(ENABLE_HEXAHEDRALMESH_SUPPORT) || defined(ENABLE_POLYHEDRALMESH_SUPPORT) || defined(ENABLE_TETRAHEDRALMESH_SUPPORT)
      /// Transform a volume mesh with the given transformation matrix
      template< typename VolumeMeshT >
      void transformVolumeMesh(ACG::Matrix4x4d _mat , VolumeMeshT& _mesh , OpenVolumeMesh::NormalAttrib<VolumeMeshT>& _normalAttrib );

      /// Calculate center of gravity of a volume mesh
      template< typename VolumeMeshT >
      ACG::Vec3d cogVolumeMesh( VolumeMeshT& _mesh );

      /// get bounding box diagonal of a volume mesh
      template< typename VolumeMeshT >
      void getBBVolumeMesh( VolumeMeshT& _mesh, ACG::Vec3d& _bb_min, ACG::Vec3d& _bb_max  );

      /// scale volume mesh to have a boundingboxdiagonal of one
      template< typename VolumeMeshT >
      void unifyBBVolumeMesh(VolumeMeshT& _mesh, OpenVolumeMesh::NormalAttrib<VolumeMeshT>& _normalAttrib, Unificationtype u = MovePlugin::DIAGONAL);

      /// Scales volume mesh such that bounding box diagonal has unit length
      template< typename VolumeMeshT >
      void unifyBBVolumeMesh( VolumeMeshT& _mesh, OpenVolumeMesh::NormalAttrib<VolumeMeshT>& _normalAttrib, ACG::Vec3d& _bb_min, ACG::Vec3d& _bb_max, Unificationtype u = MovePlugin::DIAGONAL  );
    #endif

    /** Get the Matrix of the last active Manipulator ( Identity if not found or hidden Manipulator )
      *
      * @param _reset reset the transformation matrix of the manipulator to identity)
      * @return current transformation matrix of the manipulator
      */
    ACG::Matrix4x4d getLastManipulatorMatrix(bool _reset = true);

    /// Checks if the manipulators should be visible or not
    void showManipulators( );

    /// Place and show the Manipulator
    void placeManip(QMouseEvent * _event, bool _snap = false);

    /// scale mesh to have a boundingboxdiagonal of one
    template< typename MeshT >
    void unifyBB(MeshT& _mesh , Unificationtype u = MovePlugin::DIAGONAL);

    /// get bounding box diagonal of a mesh
    template< typename MeshT >
    void getBB( MeshT& _mesh, ACG::Vec3d& _bb_min, ACG::Vec3d& _bb_max  );

    /// Scales object such that bounding box diagonal has unit length
    template< typename MeshT >
    void unifyBB( MeshT& _mesh, ACG::Vec3d& _bb_min, ACG::Vec3d& _bb_max, Unificationtype u = MovePlugin::DIAGONAL  );

    /// Size for the manipulators
    double manip_size_;

    /// Modifier for the Size (changed by Mousewheel Events)
    double manip_size_modifier_;

    /// Stores the last manipulator which has been clicked ( used for the toolbox dialog)
    int lastActiveManipulator_;

    /// Set Descriptions for scriptable functions
    void setDescriptions();

    /// Move an object with given id
    void moveObject(ACG::Matrix4x4d mat, int _id);

    /// Move selection on an object with given id
    void moveSelection(ACG::Matrix4x4d mat, int _id, QEvent::Type _type);

    /// Object marker to dimm Objects during manipulator transformation
    MoveObjectMarker objectMarker_;

  private:

    /// Holds the current manipulator mode
    QtTranslationManipulatorNode::ManipulatorMode manMode_;

    void setManipMode (QtTranslationManipulatorNode::ManipulatorMode _mode);

/** @} */

//===========================================================================
/** @name Selection Plugin Communication
  * @{ */
//===========================================================================

    /// Current SelectionType of SelectionPlugin
    SelectionType selectionType_;

    /// Get current primitive selection
    void updateSelectionType();

  private slots:

    /// Sets whether all targets should be affected or not
    void setAllTargets(bool _state);

/** @} */

  //===========================================================================
  /** @name Context Menu
   * @{ */
  //===========================================================================

    private slots:

    /// Show properties of move manipulator in a dialog ( Called via context for picking. Get the picked id from the Qvariant attached to the connected action)
    void showProps( );
    
    /// Hide the manipulator( Called via context for picking. Get the picked id from the Qvariant attached to the connected action)
    void hideManipulator();

    /// Get properties dialog widget that is attached to BaseDataObject obj
    movePropsWidget* getDialogWidget(BaseObjectData* _obj);

    /// Get parent properties dialog widget of QPushButton but
    movePropsWidget* getDialogFromButton(QPushButton* _but);

    /** Override ContextMenuInterface slot in order to avoid appearance of
     * context menu entry when clicking on other nodes (other than manpipulator nodes)
     */
    void slotUpdateContextMenuNode( int _nodeId ) override;

    private:

    void setPickModeProps(movePropsWidget* _pW, const std::string &_pickmode);

    /// List of properties dialogs (each corresponding to one manipulator)
    QList<movePropsWidget*> propsWindows_;

    /// Context menu entry for showing per manipulator settings
    QAction* contextAction_;
    
    /// Context menu entry to hide a manipulator
    QAction* contextActionHide_;

    /// Checked if transformation should be applied to all target objs
    QAction* toAllTargets_;

    /// Additional Context Menu replicating the toolbar stuff
    QMenu* contextMenuManipControl_;

    /// Action holding the context menu for toolbar replication
    QAction* contextMenuManipControlsAction_;

/** @} */

//===========================================================================
/** @name Scriptable Functions
  * @{ */
//===========================================================================

public slots :

  /// Move target Meshes cog to the origin
  void slotMoveToOrigin();

  /// Scale Boundingbox Diagonal to unit size
  void slotUnifyBoundingBoxDiagonal();

  /// Scale Boundingbox longest axis to unit size (keeps aspect ratio)
  void slotUnifyBoundingBoxLongestAxis();

  /// Scale all Boundingbox axis to unit size
  void slotUnifyBoundingBoxAllAxis();

  /// translate an Object by a given vector
  void translate( int _objectId , Vector _vector );

  /// translate given vertices by a vector
  void translate( int _objectId , IdList _vHandles, Vector _vector );

  /// translate current vertex selection of an Object by a given vector
  void translateVertexSelection( int _objectId , Vector _vector );

  /// translate current face selection of an Object by a given vector
  void translateFaceSelection( int _objectId , Vector _vector );

  /// translate current edge selection of an Object by a given vector
  void translateEdgeSelection( int _objectId , Vector _vector );

  /// transform an Object by a given matrix
  void transform( int _objectId , Matrix4x4 _matrix );

  /// transform given vertices by a matrix
  void transform( int _objectId , IdList _vHandles, Matrix4x4 _matrix );

  /// transform current selection of an Object by a given matrix
  bool transformVertexSelection( int _objectId , Matrix4x4 _matrix );

  /// transform current selection of an Object by a given matrix
  bool transformFaceSelection( int _objectId , Matrix4x4 _matrix );

  /// transform current selection of an Object by a given matrix
  bool transformEdgeSelection( int _objectId , Matrix4x4 _matrix );

  /// transform current selection of an Object by a given matrix
  bool transformCellSelection( int _objectId , Matrix4x4 _matrix );

  /// Transform handle region using the given transformation matrix
  void transformHandleRegion(int _objectId, Matrix4x4 _matrix);

  /// Set the position of the manipulator
  void setManipulatorPosition( int _objectId , Vector _position );

  /// Get the position of the manipulator
  Vector manipulatorPosition( int _objectId );

  /// Set the direction of the manipulator
  void setManipulatorDirection( int _objectId , Vector _directionX, Vector _directionY );

  /// Get the x-direction of the manipulator
  Vector manipulatorDirectionX( int _objectId );

  /// Get the y-direction of the manipulator
  Vector manipulatorDirectionY( int _objectId );

  /// Get the z-direction of the manipulator
  Vector manipulatorDirectionZ( int _objectId );

  /** \brief Sets the Object Matrix in the scenegraph to identity
   */
  void objectRenderingMatrixIdentity(int _objectId);

  /** \brief Adds a scaling factor to the Object rendering Matrix in the scenegraph
   *
   *  @param _objectId Id of the object to modify
   *  @param _s        Scaling factor
   */
  void objectRenderingMatrixScale(int _objectId, double _s);

  /** \brief Adds a scaling factor to the Object rendering Matrix in the scenegraph
   *
   *  @param _objectId    Id of the object to modify
   *  @param _translation Translation vector
   */
  void objectRenderingMatrixTranslate(int _objectId, Vector _translation);

  /** \brief Adds a scaling factor to the Object rendering Matrix in the scenegraph
     *
     *  @param _objectId Id of the object to modify
     *  @param _axis     Rotation axis
     *  @param _angle    Rotation angle
     */
  void objectRenderingMatrixRotate(int _objectId, Vector _axis, double _angle);

  /** \brief Gets the Object Matrix in the scenegraph
   *
   *  The object itself is not modified b< this matrix. Only the transformation matrix applied to the object
   *  coordinates before rendering gets modified.
   *
   *  @param _objectId Id of the object to modify
   *  @return Current Matrix of the object
   */
  Matrix4x4 getObjectRenderingMatrix(int _objectId);

/** @} */

  public slots:
    QString version() override { return QString("1.2"); };

  private:
    bool hide_;

    bool allTargets_;

    bool placeMode_;

    /// stores if any selected elements where transformed
    bool transformedSelected_;
};


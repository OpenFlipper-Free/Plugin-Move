/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/



#include "MovePlugin.hh"

#ifdef ENABLE_POLYLINE_SUPPORT
#include <ObjectTypes/PolyLine/PolyLine.hh>
#endif
#ifdef ENABLE_TSPLINEMESH_SUPPORT
#include <ObjectTypes/TSplineMesh/TSplineMesh.hh>
#endif
#include <MeshTools/MeshFunctions.hh>

#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
#include <ObjectTypes/HexahedralMesh/HexahedralMesh.hh>
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
#include <ObjectTypes/TetrahedralMesh/TetrahedralMesh.hh>
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
#include <ObjectTypes/PolyhedralMesh/PolyhedralMesh.hh>
#endif

/** \brief Set Descriptions for Scripting Slots
 *
 */
void MovePlugin::setDescriptions(){

  emit setSlotDescription("translate(int,Vector)",tr("Translate object by given vector."),
                          QString(tr("objectId,Vector")).split(","), QString(tr("ID of an object, translation vector")).split(","));

  emit setSlotDescription("translate(int,idList,Vector)",tr("Translate vertices by given vector."),
                          QString(tr("objectId,VertexHandles,Vector")).split(","),
                          QString(tr("ID of an object, List of vertex handles, translation vector")).split(","));

  emit setSlotDescription("translateVertexSelection(int,Vector)",tr("Translate current vertex selection of an object by given vector."),
                          QString(tr("objectId,Vector")).split(","), QString(tr("ID of an object, translation vector")).split(","));

  emit setSlotDescription("translateFaceSelection(int,Vector)",tr("Translate current face selection of an object by given vector."),
                          QString(tr("objectId,Vector")).split(","), QString(tr("ID of an object, translation vector")).split(","));

  emit setSlotDescription("translateEdgeSelection(int,Vector)",tr("Translate current edge selection of an object by given vector."),
                          QString(tr("objectId,Vector")).split(","), QString(tr("ID of an object, translation vector")).split(","));

  emit setSlotDescription("transformHandleRegion(int,Matrix4x4)",tr("Transform handle region using the specified matrix."),
                          QString(tr("objectId,Matrix")).split(","), QString(tr("ID of an object, transformation matrix")).split(","));

  emit setSlotDescription("transform(int,Matrix4x4)",tr("transform object by given matrix."),
                          QString(tr("objectId,Matrix")).split(","), QString(tr("ID of an object, transformation matrix")).split(","));

  emit setSlotDescription("transform(int,IdList,Matrix4x4)",tr("transform vertices by given matrix."),
                          QString(tr("objectId,VertexHandles,Matrix")).split(","),
                          QString(tr("ID of an object, List of vertex handles, transformation matrix")).split(","));

  emit setSlotDescription("transformSelection(int,Matrix4x4)",tr("transform current selection of an object by given matrix."),
                          QString(tr("objectId,Matrix")).split(","), QString(tr("ID of an object, transformation matrix")).split(","));

  emit setSlotDescription("transformCellSelection(int,Matrix4x4)",tr("transform selected cells by given matrix."),
                          QString(tr("objectId,Matrix")).split(","),
                          QString(tr("ID of an object, transformation matrix")).split(","));

  emit setSlotDescription("transformVertexSelection(int,Matrix4x4)",tr("transform selected vertices by given matrix."),
                          QString(tr("objectId,Matrix")).split(","),
                          QString(tr("ID of an object, transformation matrix")).split(","));

  emit setSlotDescription("transformFaceSelection(int,Matrix4x4)",tr("transform selected faces by given matrix."),
                          QString(tr("objectId,Matrix")).split(","),
                          QString(tr("ID of an object, transformation matrix")).split(","));

  emit setSlotDescription("transformEdgeSelection(int,Matrix4x4)",tr("transform selected edges by given matrix."),
                          QString(tr("objectId,Matrix")).split(","),
                          QString(tr("ID of an object, transformation matrix")).split(","));

  emit setSlotDescription("setManipulatorPosition(int,Vector)",tr("Set the position of the manipulator."),
                          QString(tr("objectId,Position")).split(","), QString(tr("ID of an object, 3D point")).split(","));

  emit setSlotDescription("setManipulatorDirection(int,Vector, Vector)",tr("Set the direction of the manipulator."),
                          QString(tr("objectId,Direction, Direction")).split(","), QString(tr("ID of an object, x-direction, y-direction")).split(","));

  emit setSlotDescription("manipulatorPosition(int)",tr("Returns the position of an object's manipulator."),
                          QStringList(tr("objectId")), QStringList(tr("ID of an object")));

  emit setSlotDescription("manipulatorDirectionX(int)",tr("Returns the x-direction of an object's manipulator."),
                          QStringList(tr("objectId")), QStringList(tr("ID of an object")));

  emit setSlotDescription("manipulatorDirectionY(int)",tr("Returns the y-direction of an object's manipulator."),
                          QStringList(tr("objectId")), QStringList(tr("ID of an object")));

  emit setSlotDescription("manipulatorDirectionZ(int)",tr("Returns the z-direction of an object's manipulator."),
                          QStringList(tr("objectId")), QStringList(tr("ID of an object")));


  emit setSlotDescription("objectRenderingMatrixIdentity(int)",tr("Resets  the objects rendering matrix to identity."),
                          QStringList(tr("objectId")), QStringList(tr("ID of an object")));


  emit setSlotDescription("objectRenderingMatrixScale(int,double)",tr("Adds a scaling factor to the Object rendering Matrix in the scenegraph."),
                          QStringList(tr("objectId;Scaling Factor").split(";")), QStringList(tr("ID of an object; Scaling factor").split(";")));

  emit setSlotDescription("objectRenderingMatrixTranslate(int,Vector)",tr("Adds a translation to the Object rendering Matrix in the scenegraph."),
                          QStringList(tr("objectId;translation vector").split(";")), QStringList(tr("ID of an object;Translation vector").split(";")));

  emit setSlotDescription("objectRenderingMatrixRotate(int,Vector,double)",tr("Adds a Rotation to the Object rendering Matrix in the scenegraph."),
                          QStringList(tr("objectId;rotation axis;angle").split(";")), QStringList(tr("ID of an object;Rotation axis;angle").split(";")));

  emit setSlotDescription("getObjectRenderingMatrix(int)",tr("Returns the current object transformation matrix from the scenegraph."),
                            QStringList(tr("objectId").split(";")), QStringList(tr("ID of an object").split(";")));


}


//------------------------------------------------------------------------------

/** \brief Translate an object
 *
 * @param _objectId id of the object
 * @param _vector translation vector
 */
void MovePlugin::translate( int _objectId , Vector _vector) {

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("translate : unable to get object") );
    return;
  }

  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));
    for (auto v_it : mesh.vertices())
      mesh.set_point(v_it,mesh.point(v_it) + _vector );

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));
    for (auto v_it : mesh.vertices())
      mesh.set_point(v_it,mesh.point(v_it) + _vector );

  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));
    for (auto v_it : mesh.vertices())
      mesh.set_point(v_it,mesh.point(v_it) + _vector );

  }
#endif
#ifdef ENABLE_POLYLINE_SUPPORT
  else if ( object->dataType(DATA_POLY_LINE) ) {
    
    PolyLine& line = (* PluginFunctions::polyLine(object) );
    
    for ( int i = 0 ; i < (int)line.n_vertices(); ++i )
      line.point(i) = line.point(i)  + _vector; 
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_HEXAHEDRAL_MESH) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    for (auto v_it : mesh.vertices())
      mesh.set_vertex(v_it, mesh.vertex(v_it) + _vector );
  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_TETRAHEDRAL_MESH) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    for (auto v_it : mesh.vertices())
      mesh.set_vertex(v_it, mesh.vertex(v_it) + _vector );
  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_POLYHEDRAL_MESH) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    for (auto v_it : mesh.vertices())
      mesh.set_vertex(v_it, mesh.vertex(v_it) + _vector );
  }
#endif
#ifdef ENABLE_BSPLINE_CURVE_SUPPORT
  else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
    std::cerr << "Todo : translate BSplineCurve" << std::endl;
  }
#endif

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  emit scriptInfo( "translate( ObjectId , Vector(" +
                   QString::number( _vector[0] ) + " , " +
                   QString::number( _vector[1] ) + " , " +
                   QString::number( _vector[2] ) + " ) )" );
}


//------------------------------------------------------------------------------

/** \brief translate a set of vertex handles
 *
 * @param _objectId id of an object
 * @param _vHandles list of vertex handles
 * @param _vector translation vector
 */
void MovePlugin::translate( int _objectId , IdList _vHandles, Vector _vector ){
  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("translate : unable to get object") );
    return;
  }

  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));

    for (uint i=0; i < _vHandles.size(); i++){
      TriMesh::VertexHandle vh( _vHandles[i] );
      mesh.set_point(vh  ,mesh.point( vh ) + _vector );
    }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));

    for (uint i=0; i < _vHandles.size(); i++){
      PolyMesh::VertexHandle vh( _vHandles[i] );
      mesh.set_point(vh  ,mesh.point( vh ) + _vector );
    }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
   else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));

    for (uint i=0; i < _vHandles.size(); i++){
      TSplineMesh::VertexHandle vh( _vHandles[i] );
      mesh.set_point(vh  ,mesh.point( vh ) + _vector );
    }
   }
#endif
#ifdef ENABLE_POLYLINE_SUPPORT
  else if ( object->dataType(DATA_POLY_LINE) ) {
    
    PolyLine& line = (* PluginFunctions::polyLine(object) );
    
    const int max = line.n_vertices();
    
    for ( unsigned int i = 0 ; i < _vHandles.size(); ++i ) 
      if ( (_vHandles[i] > 0) && ( _vHandles[i] < max ) )
        line.point( _vHandles[i] ) = line.point( _vHandles[i] )  + _vector; 
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_HEXAHEDRAL_MESH) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    for (unsigned int i = 0; i < _vHandles.size(); ++i) {
      OpenVolumeMesh::VertexHandle v(_vHandles[i]);
      mesh.set_vertex(v, mesh.vertex(v) + _vector );
    }
  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_TETRAHEDRAL_MESH) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    for (unsigned int i = 0; i < _vHandles.size(); ++i) {
      OpenVolumeMesh::VertexHandle v(_vHandles[i]);
      mesh.set_vertex(v, mesh.vertex(v) + _vector );
    }
  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_POLYHEDRAL_MESH) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    for (unsigned int i = 0; i < _vHandles.size(); ++i) {
      OpenVolumeMesh::VertexHandle v(_vHandles[i]);
      mesh.set_vertex(v, mesh.vertex(v) + _vector );
    }
  }
#endif
#ifdef ENABLE_BSPLINE_CURVE_SUPPORT
  else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
    std::cerr << "Todo : translate BSplineCurve" << std::endl;
  }
#endif

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  emit scriptInfo( "translate( ObjectId , Vector(" +
                   QString::number( _vector[0] ) + " , " +
                   QString::number( _vector[1] ) + " , " +
                   QString::number( _vector[2] ) + " ) )" );
                   
}


//------------------------------------------------------------------------------

/** \brief translate vertex selection
 *
 * @param _objectId id of an object
 * @param _vector translation vector
 */
void MovePlugin::translateVertexSelection( int _objectId , Vector _vector) {

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("translate : unable to get object" ));
    return;
  }

  bool noneSelected = true;

  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));
    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).selected() ) {
        noneSelected = false;
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
      }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));
    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).selected() ) {
        noneSelected = false;
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
      }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));
    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).selected() ) {
        noneSelected = false;
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
      }
  }
#endif
#ifdef ENABLE_POLYLINE_SUPPORT
  else if ( object->dataType(DATA_POLY_LINE) ) {
    
    PolyLine& line = (* PluginFunctions::polyLine(object) );
    
    for ( int i = 0 ; i < (int)line.n_vertices(); ++i )
      if ( line.vertex_selection(i) ) {
        noneSelected = false;
        line.point(i) = line.point(i)  + _vector; 
      }
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_HEXAHEDRAL_MESH) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((HexahedralMeshObject*)object)->status();
    for (auto v_it : mesh.vertices())
      if (statusAttrib[v_it].selected()) {
        noneSelected = false;
        mesh.set_vertex(v_it, mesh.vertex(v_it) + _vector );
      }
  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_TETRAHEDRAL_MESH) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((TetrahedralMeshObject*)object)->status();
    for (auto v_it : mesh.vertices())
      if (statusAttrib[v_it].selected()) {
        noneSelected = false;
        mesh.set_vertex(v_it, mesh.vertex(v_it) + _vector );
      }
  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_POLYHEDRAL_MESH) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((PolyhedralMeshObject*)object)->status();
    for (auto v_it : mesh.vertices())
      if (statusAttrib[v_it].selected()) {
        noneSelected = false;
        mesh.set_vertex(v_it, mesh.vertex(v_it) + _vector );
      }
  }
#endif
#ifdef ENABLE_BSPLINE_CURVE_SUPPORT
  else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
    std::cerr << "Todo : translate BSplineCurve" << std::endl;
  }
#endif

  if (noneSelected)
    return;

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  emit scriptInfo( "translate( ObjectId , Vector(" +
                   QString::number( _vector[0] ) + " , " +
                   QString::number( _vector[1] ) + " , " +
                   QString::number( _vector[2] ) + " ) )" );
}


//------------------------------------------------------------------------------

/** \brief translate face selection
 *
 * @param _objectId id of an object
 * @param _vector translation vector
 */
void MovePlugin::translateFaceSelection( int _objectId , Vector _vector) {

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("translate : unable to get object" ));
    return;
  }

  bool noneSelected = true;

  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));

    // clear tags
    for (auto v_it : mesh.vertices())
      mesh.status(v_it).set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( mesh.status(f_it).selected() )
      {
        for(auto fv_it : f_it.vertices()) {
          noneSelected = false;
          mesh.status(fv_it).set_tagged(true);
        }
      }

    if (noneSelected)
      return;

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() )
        mesh.set_point(v_it,mesh.point(v_it) + _vector );

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));

    // clear tags
    for (auto v_it : mesh.vertices())
      mesh.status(v_it).set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( mesh.status(f_it).selected() )
      {
        for(auto fv_it : f_it.vertices()) {
          noneSelected = false;
          mesh.status(fv_it).set_tagged(true);
        }
      }

    if (noneSelected)
      return;

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() )
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));

    // clear tags
    for (auto v_it : mesh.vertices())
      mesh.status(v_it).set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( mesh.status(f_it).selected() )
      {
        for(auto fv_it : f_it.vertices()) {
          noneSelected = false;
          mesh.status(fv_it).set_tagged(true);
        }
      }

    if (noneSelected)
      return;

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() )
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
  }
#endif

  #ifdef ENABLE_POLYLINE_SUPPORT
    else if ( object->dataType(DATA_POLY_LINE) ) {

      PolyLine& line = (* PluginFunctions::polyLine(object) );

      for ( int i = 0 ; i < (int)line.n_vertices(); ++i )
        if ( line.vertex_selection(i) ) {
          noneSelected = false;
          line.point(i) = line.point(i)  + _vector;
        }

      if (noneSelected)
        return;
    }
  #endif
  #ifdef ENABLE_BSPLINE_CURVE_SUPPORT
    else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
      std::cerr << "Todo : translate BSplineCurve" << std::endl;
    }
  #endif

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  emit scriptInfo( "translate( ObjectId , Vector(" +
                   QString::number( _vector[0] ) + " , " +
                   QString::number( _vector[1] ) + " , " +
                   QString::number( _vector[2] ) + " ) )" );

}


//------------------------------------------------------------------------------

/** \brief translate edge selection
 *
 * @param _objectId id of an object
 * @param _vector translation vector
 */
void MovePlugin::translateEdgeSelection( int _objectId , Vector _vector) {

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("translate : unable to get object" ));
    return;
  }

  bool noneSelected = true;

  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));

    // clear tags
    for (auto v_it : mesh.vertices())
      mesh.status(v_it).set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( mesh.status(e_it).selected() )
      {
        noneSelected = false;
        OpenMesh::SmartHalfedgeHandle hh = e_it.h0();

        mesh.status( hh.from() ).set_tagged(true);
        mesh.status( hh.to() ).set_tagged(true);
      }

    if (noneSelected)
      return;

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
      }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));

    // clear tags
    for (auto v_it : mesh.vertices())
      mesh.status(v_it).set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( mesh.status(e_it).selected() )
      {
        noneSelected = false;
        OpenMesh::SmartHalfedgeHandle hh = e_it.h0();

        mesh.status( hh.from() ).set_tagged(true);
        mesh.status( hh.to() ).set_tagged(true);
      }

    if (noneSelected)
      return;

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
      }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));

    // clear tags
    for (auto v_it : mesh.vertices())
      mesh.status(v_it).set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( mesh.status(e_it).selected() )
      {
        noneSelected = false;
        OpenMesh::SmartHalfedgeHandle hh = e_it.h0();

        mesh.status( hh.from() ).set_tagged(true);
        mesh.status( hh.to() ).set_tagged(true);
      }

    if (noneSelected)
      return;

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point(v_it,mesh.point(v_it) + _vector );
      }
  }
#endif

  #ifdef ENABLE_POLYLINE_SUPPORT
    else if ( object->dataType(DATA_POLY_LINE) ) {

      PolyLine& line = (* PluginFunctions::polyLine(object) );

      for ( int i = 0 ; i < (int)line.n_vertices(); ++i )
        if ( line.vertex_selection(i) ) {
          noneSelected = false;
          line.point(i) = line.point(i)  + _vector;
        }

      if (noneSelected)
        return;
    }
  #endif
  #ifdef ENABLE_BSPLINE_CURVE_SUPPORT
    else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
      std::cerr << "Todo : translate BSplineCurve" << std::endl;
    }
  #endif

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  emit scriptInfo( "translate( ObjectId , Vector(" +
                   QString::number( _vector[0] ) + " , " +
                   QString::number( _vector[1] ) + " , " +
                   QString::number( _vector[2] ) + " ) )" );

}
//------------------------------------------------------------------------------

void MovePlugin::transformHandleRegion(int _objectId, Matrix4x4 _matrix) {

  BaseObjectData* object = NULL;
  if (!PluginFunctions::getObject(_objectId, object)) {
    emit log(LOGERR, tr("transformHandleRegion: Unable to get object!"));
    return;
  }

  if(object->dataType(DATA_TRIANGLE_MESH)) {

    TriMesh& mesh = (*PluginFunctions::triMesh(object));

    MeshFunctions::transformHandleVertices(_matrix, mesh);

  } else if(object->dataType(DATA_POLY_MESH)) {

    PolyMesh& mesh = (*PluginFunctions::polyMesh(object));

    MeshFunctions::transformHandleVertices(_matrix, mesh);
  }

  emit updatedObject(_objectId, UPDATE_GEOMETRY);
}

//------------------------------------------------------------------------------

/** \brief transform an object
 *
 * @param _objectId object id
 * @param _matrix transformation matrix
 */
void MovePlugin::transform( int _objectId , Matrix4x4 _matrix ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("transform : unable to get object" ));
    return;
  }

  Matrix4x4 normalMatrix = _matrix;
  normalMatrix.invert();
  normalMatrix.transpose();

  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));
    for (auto v_it : mesh.vertices()){
      mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
      mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
    }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));
    for (auto v_it : mesh.vertices()){
      mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
      mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
    }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));
    for (auto v_it : mesh.vertices()){
      mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
      mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
    }
  }
#endif
#ifdef ENABLE_POLYLINE_SUPPORT
  else if ( object->dataType(DATA_POLY_LINE) ) {
    
    PolyLine& line = (* PluginFunctions::polyLine(object) );
    
    for ( int i = 0 ; i < (int)line.n_vertices(); ++i )
      line.point(i) = _matrix.transform_point( line.point(i) ); 
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_HEXAHEDRAL_MESH) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<HexahedralMesh>& normalAttrib = ((HexahedralMeshObject*)object)->normals();
    for (auto v_it : mesh.vertices()) {
      mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
      normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
    }
  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_TETRAHEDRAL_MESH) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<TetrahedralMesh>& normalAttrib = ((TetrahedralMeshObject*)object)->normals();
    for (auto v_it : mesh.vertices()) {
      mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
      normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
    }
  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_POLYHEDRAL_MESH) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    OpenVolumeMesh::NormalAttrib<PolyhedralMesh>& normalAttrib = ((PolyhedralMeshObject*)object)->normals();
    for (auto v_it : mesh.vertices()) {
      mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
      normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
    }
  }
#endif
#ifdef ENABLE_BSPLINE_CURVE_SUPPORT
  else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
    std::cerr << "Todo : transform BSplineCurve" << std::endl;
  }
#endif

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  QString matString;
  for (int i=0; i < 4; i++)
    for (int j=0; j < 4; j++)
      matString += " , " + QString::number( _matrix(i,j) );

  matString = matString.right( matString.length()-3 );

  emit scriptInfo( "transform( ObjectId , Matrix4x4(" + matString + " ) )" );
}


//------------------------------------------------------------------------------

/** \brief Transform a set of vertex handles
 *
 * @param _objectId id of an object
 * @param _vHandles list of vertex handles
 * @param _matrix transformation matrix
 */
void MovePlugin::transform( int _objectId , IdList _vHandles, Matrix4x4 _matrix ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("transform : unable to get object" ));
    return;
  }

  Matrix4x4 normalMatrix = _matrix;
  normalMatrix.invert();
  normalMatrix.transpose();

  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));

    for (uint i=0; i < _vHandles.size(); i++){
      TriMesh::VertexHandle vh( _vHandles[i] );
      mesh.set_point (vh, _matrix.transform_point ( mesh.point(vh) ) );
      mesh.set_normal(vh, normalMatrix.transform_vector( mesh.normal(vh) ) );
    }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));

    for (uint i=0; i < _vHandles.size(); i++){
      PolyMesh::VertexHandle vh( _vHandles[i] );
      mesh.set_point (vh, _matrix.transform_point ( mesh.point(vh) ) );
      mesh.set_normal(vh, normalMatrix.transform_vector( mesh.normal(vh) ) );
    }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));

    for (uint i=0; i < _vHandles.size(); i++){
      TSplineMesh::VertexHandle vh( _vHandles[i] );
      mesh.set_point (vh, _matrix.transform_point ( mesh.point(vh) ) );
      mesh.set_normal(vh, normalMatrix.transform_vector( mesh.normal(vh) ) );
    }
   }
#endif
#ifdef ENABLE_POLYLINE_SUPPORT
  else if ( object->dataType(DATA_POLY_LINE) ) {
    
    PolyLine& line = (* PluginFunctions::polyLine(object) );
    
    const int max = line.n_vertices();
    
    for ( unsigned int i = 0 ; i < _vHandles.size(); ++i ) 
      if ( (_vHandles[i] > 0) && ( _vHandles[i] < max ) )
        line.point( _vHandles[i] ) = _matrix.transform_point( line.point( _vHandles[i] ) ); 
    
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_HEXAHEDRAL_MESH) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<HexahedralMesh>& normalAttrib = ((HexahedralMeshObject*)object)->normals();
    for (unsigned int i = 0; i < _vHandles.size(); ++i) {
      OpenVolumeMesh::VertexHandle v(_vHandles[i]);
      mesh.set_vertex(v, _matrix.transform_point ( mesh.vertex(v) ) );
      normalAttrib[v] = normalMatrix.transform_vector( normalAttrib[v] );
    }
  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_TETRAHEDRAL_MESH) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<TetrahedralMesh>& normalAttrib = ((TetrahedralMeshObject*)object)->normals();
    for (unsigned int i = 0; i < _vHandles.size(); ++i) {
      OpenVolumeMesh::VertexHandle v(_vHandles[i]);
      mesh.set_vertex(v, _matrix.transform_point ( mesh.vertex(v) ) );
      normalAttrib[v] = normalMatrix.transform_vector( normalAttrib[v] );
    }
  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_POLYHEDRAL_MESH) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    OpenVolumeMesh::NormalAttrib<PolyhedralMesh>& normalAttrib = ((PolyhedralMeshObject*)object)->normals();
    for (unsigned int i = 0; i < _vHandles.size(); ++i) {
      OpenVolumeMesh::VertexHandle v(_vHandles[i]);
      mesh.set_vertex(v, _matrix.transform_point ( mesh.vertex(v) ) );
      normalAttrib[v] = normalMatrix.transform_vector( normalAttrib[v] );
    }
  }
#endif
#ifdef ENABLE_BSPLINE_CURVE_SUPPORT
  else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
    std::cerr << "Todo : transform BSplineCurve" << std::endl;
  }
#endif

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  QString matString;
  for (int i=0; i < 4; i++)
    for (int j=0; j < 4; j++)
      matString += " , " + QString::number( _matrix(i,j) );

  matString = matString.right( matString.length()-3 );

  emit scriptInfo( "transform( ObjectId , Matrix4x4(" + matString + " ) )" );
  
}


//------------------------------------------------------------------------------

/** \brief transform vertex selection
 *
 * @param _objectId id of an object
 * @param _matrix transformation matrix
 *
 * @return returns true if selected elements were transformed
 */
bool MovePlugin::transformVertexSelection( int _objectId , Matrix4x4 _matrix ){
  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("transform : unable to get object") );
    return false;
  }

  Matrix4x4 normalMatrix = _matrix;
  normalMatrix.invert();
  normalMatrix.transpose();

  bool noneSelected = true;
  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));
    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).selected() )
      {
        noneSelected = false;
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));
    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).selected() )
      {
        noneSelected = false;
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
   else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));
    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).selected() )
      {
        noneSelected = false;
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }
   }
#endif
#ifdef ENABLE_POLYLINE_SUPPORT
  else if ( object->dataType(DATA_POLY_LINE) ) {
    
    PolyLine& line = (* PluginFunctions::polyLine(object) );
    
    for ( int i = 0 ; i < (int)line.n_vertices(); ++i )
      if ( line.vertex_selection(i) ) {
        noneSelected = false;
        line.point(i) = _matrix.transform_point( line.point(i) );
      }
  }
#endif
#ifdef ENABLE_BSPLINE_CURVE_SUPPORT
  else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
    std::cerr << "Todo : transform BSplineCurve" << std::endl;
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_HEXAHEDRAL_MESH) ) {
    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<HexahedralMesh>& normalAttrib = ((HexahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((HexahedralMeshObject*)object)->status();
    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].selected() )
      {
        noneSelected = false;
        mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
        normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }
  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_TETRAHEDRAL_MESH) ) {
    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<TetrahedralMesh>& normalAttrib = ((TetrahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((TetrahedralMeshObject*)object)->status();
    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].selected() )
      {
        noneSelected = false;
        mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
        normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }
  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType(DATA_POLYHEDRAL_MESH) ) {
    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    OpenVolumeMesh::NormalAttrib<PolyhedralMesh>& normalAttrib = ((PolyhedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((PolyhedralMeshObject*)object)->status();
    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].selected() )
      {
        noneSelected = false;
        mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
        normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }
  }
#endif

  if (noneSelected)
    return false;

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  QString matString;
  for (int i=0; i < 4; i++)
    for (int j=0; j < 4; j++)
      matString += " , " + QString::number( _matrix(i,j) );

  matString = matString.right( matString.length()-3 );

  emit scriptInfo( "transformVertexSelection( ObjectId , Matrix4x4(" + matString + " ) )" );

  return true;
}


//------------------------------------------------------------------------------

/** \brief transform face selection
 *
 * @param _objectId id of an object
 * @param _matrix transformation matrix
 *
 * @return returns true if selected elements were transformed
 */
bool MovePlugin::transformFaceSelection( int _objectId , Matrix4x4 _matrix ){
  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("transform : unable to get object") );
    return false;
  }

  Matrix4x4 normalMatrix = _matrix;
  normalMatrix.invert();
  normalMatrix.transpose();

  bool noneSelected = true;
  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));

    //init tags
    for (auto v_it : mesh.vertices())
        mesh.status(v_it).set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( mesh.status(f_it).selected() )
      {
        noneSelected = false;
        for(auto fv_it : f_it.vertices())
          mesh.status(fv_it).set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));

    //init tags
    for (auto v_it : mesh.vertices())
        mesh.status(v_it).set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( mesh.status(f_it).selected() )
      {
        noneSelected = false;
        for(auto fv_it : f_it.vertices())
          mesh.status(fv_it).set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));

    //init tags
    for (auto v_it : mesh.vertices())
        mesh.status(v_it).set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( mesh.status(f_it).selected() )
      {
        noneSelected = false;
        for(auto fv_it : f_it.verices())
          mesh.status(fv_it).set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  if ( object->dataType( DATA_HEXAHEDRAL_MESH ) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<HexahedralMesh>& normalAttrib = ((HexahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((HexahedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);


    for (auto f_it : mesh.faces())
      if ( statusAttrib[f_it].selected() )
      {
        noneSelected = false;
        for (OpenVolumeMesh::HalfFaceVertexIter hfv_it = mesh.hfv_iter(mesh.halfface_handle(f_it,0)); hfv_it.valid(); ++hfv_it)
            statusAttrib[*hfv_it].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }

  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  if ( object->dataType( DATA_TETRAHEDRAL_MESH ) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<TetrahedralMesh>& normalAttrib = ((TetrahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((TetrahedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( statusAttrib[f_it].selected() )
      {
        noneSelected = false;
        for (OpenVolumeMesh::HalfFaceVertexIter hfv_it = mesh.hfv_iter(mesh.halfface_handle(f_it,0)); hfv_it.valid(); ++hfv_it)
            statusAttrib[*hfv_it].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }

  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType( DATA_POLYHEDRAL_MESH ) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    OpenVolumeMesh::NormalAttrib<PolyhedralMesh>& normalAttrib = ((PolyhedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((PolyhedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto f_it : mesh.faces())
      if ( statusAttrib[f_it].selected() )
      {
        noneSelected = false;
        for (OpenVolumeMesh::HalfFaceVertexIter hfv_it = mesh.hfv_iter(mesh.halfface_handle(f_it,0)); hfv_it.valid(); ++hfv_it)
            statusAttrib[*hfv_it].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }
  }
#endif

  if (noneSelected)
    return false;

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  QString matString;
  for (int i=0; i < 4; i++)
    for (int j=0; j < 4; j++)
      matString += " , " + QString::number( _matrix(i,j) );

  matString = matString.right( matString.length()-3 );

  emit scriptInfo( "transformFaceSelection( ObjectId , Matrix4x4(" + matString + " ) )" );
  
  return true;
}


//------------------------------------------------------------------------------

/** \brief transform edge selection
 *
 * @param _objectId id of an object
 * @param _matrix transformation matrix
 *
 * @return returns true if selected elements were transformed
 */
bool MovePlugin::transformEdgeSelection( int _objectId , Matrix4x4 _matrix ){
  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("transform : unable to get object" ) );
    return false;
  }

  Matrix4x4 normalMatrix = _matrix;
  normalMatrix.invert();
  normalMatrix.transpose();

  bool noneSelected = true;
  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh&  mesh  = (*PluginFunctions::triMesh(object));

    //init tags
    for (auto v_it : mesh.vertices())
        mesh.status(v_it).set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( mesh.status(e_it).selected() )
      {
        noneSelected = false;
        OpenMesh::SmartHalfedgeHandle hh = e_it.h0();

        mesh.status( hh.from() ).set_tagged(true);
        mesh.status( hh.to() ).set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }

  } else if ( object->dataType( DATA_POLY_MESH ) ) {

    PolyMesh&  mesh  = (*PluginFunctions::polyMesh(object));

    //init tags
    for (auto v_it : mesh.vertices())
        mesh.status(v_it).set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( mesh.status(e_it).selected() )
      {
        noneSelected = false;
        OpenMesh::SmartHalfedgeHandle hh = e_it.h0();

        mesh.status( hh.from() ).set_tagged(true);
        mesh.status( hh.to() ).set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }
  }
#ifdef ENABLE_TSPLINEMESH_SUPPORT
  else if ( object->dataType( DATA_TSPLINE_MESH ) ) {

    TSplineMesh&  mesh  = (*PluginFunctions::tsplineMesh(object));

    //init tags
    for (auto v_it : mesh.vertices())
        mesh.status(v_it).set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( mesh.status(e_it).selected() )
      {
        noneSelected = false;
        OpenMesh::SmartHalfedgeHandle hh = e_it.h0();

        mesh.status( hh.from() ).set_tagged(true);
        mesh.status( hh.to() ).set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( mesh.status(v_it).tagged() ){
        mesh.set_point (v_it, _matrix.transform_point ( mesh.point(v_it) ) );
        mesh.set_normal(v_it, normalMatrix.transform_vector( mesh.normal(v_it) ) );
      }
  }
#endif
#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  if ( object->dataType( DATA_HEXAHEDRAL_MESH ) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<HexahedralMesh>& normalAttrib = ((HexahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((HexahedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( statusAttrib[e_it].selected() )
      {
        noneSelected = false;
        OpenVolumeMesh::OpenVolumeMeshEdge e(mesh.edge(e_it));
        statusAttrib[e.from_vertex()].set_tagged(true);
        statusAttrib[e.to_vertex()].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }

  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  if ( object->dataType( DATA_TETRAHEDRAL_MESH ) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<TetrahedralMesh>& normalAttrib = ((TetrahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((TetrahedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( statusAttrib[e_it].selected() )
      {
        noneSelected = false;
        OpenVolumeMesh::OpenVolumeMeshEdge e(mesh.edge(e_it));
        statusAttrib[e.from_vertex()].set_tagged(true);
        statusAttrib[e.to_vertex()].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }

  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  if ( object->dataType( DATA_POLYHEDRAL_MESH ) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    OpenVolumeMesh::NormalAttrib<PolyhedralMesh>& normalAttrib = ((PolyhedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((PolyhedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto e_it : mesh.edges())
      if ( statusAttrib[e_it].selected() )
      {
        noneSelected = false;
        OpenVolumeMesh::OpenVolumeMeshEdge e(mesh.edge(e_it));
        statusAttrib[e.from_vertex()].set_tagged(true);
        statusAttrib[e.to_vertex()].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }

  }
#endif
  
  #ifdef ENABLE_POLYLINE_SUPPORT
    else if ( object->dataType(DATA_POLY_LINE) ) {
      std::cerr << "Todo : transform PolyLine" << std::endl;
    }
  #endif
  #ifdef ENABLE_BSPLINE_CURVE_SUPPORT
    else if ( object->dataType(DATA_BSPLINE_CURVE) ) {
      std::cerr << "Todo : transform BSplineCurve" << std::endl;
    }
  #endif

  if (noneSelected)
    return false;

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  QString matString;
  for (int i=0; i < 4; i++)
    for (int j=0; j < 4; j++)
      matString += " , " + QString::number( _matrix(i,j) );

  matString = matString.right( matString.length()-3 );

  emit scriptInfo( "transformEdgeSelection( ObjectId , Matrix4x4(" + matString + " ) )" );
  
  return true;
}

//------------------------------------------------------------------------------

/** \brief transform cell selection
 *
 * @param _objectId id of an object
 * @param _matrix transformation matrix
 *
 * @return returns true if selected elements were transformed
 */
bool MovePlugin::transformCellSelection( int _objectId , Matrix4x4 _matrix ){
  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("transform : unable to get object") );
    return false;
  }

  Matrix4x4 normalMatrix = _matrix;
  normalMatrix.invert();
  normalMatrix.transpose();

  bool noneSelected = true;

#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  if ( object->dataType( DATA_HEXAHEDRAL_MESH ) ) {

    HexahedralMesh& mesh = (*PluginFunctions::hexahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<HexahedralMesh>& normalAttrib = ((HexahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((HexahedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto c_it : mesh.cells())
      if ( statusAttrib[c_it].selected() )
      {
        noneSelected = false;
        for (OpenVolumeMesh::CellVertexIter cv_it = mesh.cv_iter(c_it); cv_it.valid(); ++cv_it)
            statusAttrib[*cv_it].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }

  }
#endif
#ifdef ENABLE_TETRAHEDRALMESH_SUPPORT
  if ( object->dataType( DATA_TETRAHEDRAL_MESH ) ) {

    TetrahedralMesh& mesh = (*PluginFunctions::tetrahedralMesh(object));
    OpenVolumeMesh::NormalAttrib<TetrahedralMesh>& normalAttrib = ((TetrahedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((TetrahedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto c_it : mesh.cells())
      if ( statusAttrib[c_it].selected() )
      {
        noneSelected = false;
        for (OpenVolumeMesh::CellVertexIter cv_it = mesh.cv_iter(c_it); cv_it.valid(); ++cv_it)
            statusAttrib[*cv_it].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }

  }
#endif
#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  else if ( object->dataType( DATA_POLYHEDRAL_MESH ) ) {

    PolyhedralMesh& mesh = (*PluginFunctions::polyhedralMesh(object));
    OpenVolumeMesh::NormalAttrib<PolyhedralMesh>& normalAttrib = ((PolyhedralMeshObject*)object)->normals();
    OpenVolumeMesh::StatusAttrib& statusAttrib = ((PolyhedralMeshObject*)object)->status();

    //init tags
    for (auto v_it : mesh.vertices())
        statusAttrib[v_it].set_tagged(false);

    for (auto c_it : mesh.cells())
      if ( statusAttrib[c_it].selected() )
      {
        noneSelected = false;
        for (OpenVolumeMesh::CellVertexIter cv_it = mesh.cv_iter(c_it); cv_it.valid(); ++cv_it)
            statusAttrib[*cv_it].set_tagged(true);
      }

    for (auto v_it : mesh.vertices())
      if ( statusAttrib[v_it].tagged() )
      {
          mesh.set_vertex(v_it, _matrix.transform_point ( mesh.vertex(v_it) ) );
          normalAttrib[v_it] = normalMatrix.transform_vector( normalAttrib[v_it] );
      }
  }
#endif

  if (noneSelected)
    return false;

  emit updatedObject(_objectId, UPDATE_GEOMETRY);

  QString matString;
  for (int i=0; i < 4; i++)
    for (int j=0; j < 4; j++)
      matString += " , " + QString::number( _matrix(i,j) );

  matString = matString.right( matString.length()-3 );

  emit scriptInfo( "transformCellSelection( ObjectId , Matrix4x4(" + matString + " ) )" );

  return true;
}


//------------------------------------------------------------------------------

/** \brief set the position of the manipulator
 *
 * @param _objectId id of an object
 * @param _position new position
 */
void MovePlugin::setManipulatorPosition( int _objectId , Vector _position ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("setManipulatorPosition : unable to get object") );
    return;
  }


  manip_size_ = PluginFunctions::sceneRadius() * 0.1;


  object->manipPlaced( true );

  object->manipulatorNode()->loadIdentity();
  object->manipulatorNode()->set_center(_position);
  object->manipulatorNode()->set_draw_cylinder(true);
  object->manipulatorNode()->set_size(manip_size_ * manip_size_modifier_);
  object->manipulatorNode()->show();

  connect(object->manipulatorNode() , SIGNAL(manipulatorMoved(QtTranslationManipulatorNode*,QMouseEvent*)),
          this                      , SLOT(  manipulatorMoved(QtTranslationManipulatorNode*,QMouseEvent*)));

  connect(object->manipulatorNode() , SIGNAL(positionChanged(QtTranslationManipulatorNode*)),
          this                      , SLOT(  ManipulatorPositionChanged(QtTranslationManipulatorNode*)));

  lastActiveManipulator_ = object->id();

  emit updateView();

  emit scriptInfo( "setManipulatorPosition( ObjectId , Vector(" +
                   QString::number( _position[0] ) + " , " +
                   QString::number( _position[1] ) + " , " +
                   QString::number( _position[2] ) + " ) )" );
}


//------------------------------------------------------------------------------

/** \brief Get the position of the manipulator
 *
 * @param _objectId id of an object
 * @return current manipulator position
 */
Vector MovePlugin::manipulatorPosition( int _objectId ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("manipulatorPosition : unable to get object" ));
    return Vector();
  }

  return (Vector) object->manipulatorNode()->center();
}


//------------------------------------------------------------------------------

/** \brief set the direction of the manipulator
 *
 * @param _objectId id of an object
 * @param _directionX vector for the X direction
 * @param _directionY vector for the Y direction
 */
void MovePlugin::setManipulatorDirection( int _objectId , Vector _directionX, Vector _directionY ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("setManipulatorDirection : unable to get object") );
    return;
  }

  if ( !object->manipPlaced() ){
    emit log(LOGERR,tr("setManipulatorDirection : manipulator position has to be set first" ));
    return;
  }

  object->manipulatorNode()->set_direction(_directionX, _directionY);

  emit scriptInfo( "setManipulatorDirection( ObjectId , Vector(" +
                   QString::number( _directionX[0] ) + " , " +
                   QString::number( _directionX[1] ) + " , " +
                   QString::number( _directionX[2] ) + " ), Vector(" +
                   QString::number( _directionY[0] ) + " , " +
                   QString::number( _directionY[1] ) + " , " +
                   QString::number( _directionY[2] ) + " ) )" );
}


//------------------------------------------------------------------------------

/** \brief Get the x-direction of the manipulator
 *
 * @param _objectId id of an object
 * @return x-direction of the manipulator
 */
Vector MovePlugin::manipulatorDirectionX( int _objectId ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("manipulatorDirection : unable to get object" ));
    return Vector();
  }

  return (Vector) object->manipulatorNode()->directionX();
}


//------------------------------------------------------------------------------

/** \brief Get the y-direction of the manipulator
 *
 * @param _objectId id of an object
 * @return y-direction of the manipulator
 */
Vector MovePlugin::manipulatorDirectionY( int _objectId ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("manipulatorDirection : unable to get object" ));
    return Vector();
  }

  return (Vector) object->manipulatorNode()->directionY();
}


//------------------------------------------------------------------------------

/** \brief Get the z-direction of the manipulator
 *
 * @param _objectId id of an object
 * @return z-direction of the manipulator
 */
Vector MovePlugin::manipulatorDirectionZ( int _objectId ){

  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("manipulatorDirection : unable to get object" ));
    return Vector();
  }

  return (Vector) object->manipulatorNode()->directionZ();
}

//------------------------------------------------------------------------------

void MovePlugin::objectRenderingMatrixIdentity(int _objectId) {
  BaseObjectData* object;

  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("objectRenderingMatrixIdentity : unable to get object" ));
    return ;
  }

  object->manipulatorNode()->loadIdentity();

  emit updatedObject(_objectId,UPDATE_VISIBILITY);
}

//------------------------------------------------------------------------------

void MovePlugin::objectRenderingMatrixScale(int _objectId, double _s) {
  BaseObjectData* object;

  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("objectRenderingMatrixScale : unable to get object" ));
    return ;
  }

  object->manipulatorNode()->scale(_s);

  emit updatedObject(_objectId,UPDATE_VISIBILITY);
}

//------------------------------------------------------------------------------

void MovePlugin::objectRenderingMatrixTranslate(int _objectId, Vector _translation) {
  BaseObjectData* object;

  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("objectRenderingMatrixTranslate : unable to get object" ));
    return ;
  }

  object->manipulatorNode()->translate(_translation);

  emit updatedObject(_objectId,UPDATE_VISIBILITY);
}

//------------------------------------------------------------------------------

void MovePlugin::objectRenderingMatrixRotate(int _objectId, Vector _axis, double _angle) {
  BaseObjectData* object;

  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("objectRenderingMatrixRotate : unable to get object" ));
    return ;
  }

  object->manipulatorNode()->rotate(_angle,_axis);

  emit updatedObject(_objectId,UPDATE_VISIBILITY);
}

//------------------------------------------------------------------------------

Matrix4x4 MovePlugin::getObjectRenderingMatrix(int _objectId) {
  BaseObjectData* object;

  if ( ! PluginFunctions::getObject(_objectId,object) ) {
    emit log(LOGERR,tr("getObjectRenderingMatrix : unable to get object" ));
    return Matrix4x4();
  }

  return object->manipulatorNode()->matrix();
}

